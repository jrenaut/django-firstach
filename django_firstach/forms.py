from django import forms
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit

class BaseBootstrapForm(forms.Form):
	def __init__(self, *args, **kwargs):
		super(BaseBootstrapForm, self).__init__(*args, **kwargs)
		self.helper = FormHelper()
		self.helper.form_class = 'form-horizontal'
		self.helper.label_class = 'col-lg-3'
		self.helper.field_class = 'col-lg-9'
		self.helper.form_method = 'post'
		self.helper.add_input(Submit('submit', 'Submit'))

class CustomerProfileForm(BaseBootstrapForm):
	#Name of Customer
	customerName = forms.CharField(max_length=50, label="Name")
	#Merchant-specified identifier of Customer
	#customerID = forms.CharField(max_length=50, required=False)
	#Address of Customer
	address1 = forms.CharField(max_length=100, label="Address")
	#City of Customer
	city = forms.CharField(max_length=50, label="City")
	#Valid 2-character state abbreviation
	state = forms.CharField(max_length=2, label="State")
	zip = forms.CharField(max_length=5, label="Zip Code")
	#Phone number of Customer: xxx-xxx-xxxx
	phone = forms.CharField(max_length=12, label="Phone Number")
	email = forms.EmailField(max_length=50, required=False, label="Email")
	fax = forms.CharField(max_length=12, required=False, label="Fax Number")
	drvLic = forms.CharField(max_length=50, required=False, label="Drivers License Number")
	drvState = forms.CharField(max_length=2, required=False, label="License State")
	#Permitted if custom field has been previously enabled
	custom1 = forms.CharField(max_length=100, required=False, label="Custom")
	#Permitted if custom field has been previously enabled
	custom2 = forms.CharField(max_length=100, required=False, label="Custom 2")
	def __init__(self, *args, **kwargs):
		super(CustomerProfileForm, self).__init__(*args, **kwargs)
		self.helper.form_id = 'id-customerprofileform'

ACCT_TYPE = (('Personal Checking','Personal Checking'),('Business Checking', 'Business Checking'), ('Personal Savings','Personal Savings'))
class CustomerAccountForm(BaseBootstrapForm):
	accountName = forms.CharField(max_length=50, label="Account Name")
	accountType = forms.ChoiceField(choices=ACCT_TYPE, label="Account Type")
	routingNumber = forms.CharField(max_length=9, label= "Routing Number")
	accountNumber = forms.CharField(max_length=20, label = "Account Number", help_text="We store the last four digits to identify your account")
	isDefault = forms.BooleanField(label="Default?", initial=False, required=False, help_text="Make this your default payment method")	
	def __init__(self, *args, **kwargs):
		super(CustomerAccountForm, self).__init__(*args, **kwargs)
		self.helper.form_id = 'id-customeraccountform'


SECC_TYPE=(('CCD', 'CCD'), ('PPD', 'PPD'), ('ARC', 'ARC'), ('RCK', 'RCK'), ('TEL', 'TEL'), ('WEB', 'WEB'))	
PAYMENT_TYPE = (('DEBIT', 'Debit'), ('CREDIT', 'Credit'))
FREQUENCY = ((1,'Once'), (2,'Weekly'), (3,'Biweekly'), (4,'Monthly'), (5,'Quarterly'), (6,'Semiannually'), (7,'Annually'))
class CustomerPaymentForm(BaseBootstrapForm):
	SECCType = forms.ChoiceField(choices=SECC_TYPE, label="Authorization Method")
	checkNo = forms.CharField(max_length=5, required=False) #required for SECCType ARC or RCK
	paymentType = forms.ChoiceField(choices=PAYMENT_TYPE, label="Payment Type")
	amount = forms.DecimalField(min_value=0, decimal_places=2)
	effecDate = forms.DateField(label="Effective Date")
	frequency = forms.ChoiceField(choices=FREQUENCY, label="Frequency")
	numberOfPayments = forms.IntegerField(label="Number of Payments", min_value=1, max_value=9998)# use 9999 for unlimited
	memo = forms.CharField(max_length=255, required=False)		
	"""
	isPrivate
	custom1"""
	def __init__(self, *args, **kwargs):
		super(CustomerPaymentForm, self).__init__(*args, **kwargs)
		self.helper.form_id = 'id-customerpaymentform'
		#self.helper.form_action = 'some action'

	
	
