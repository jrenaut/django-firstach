from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseRedirect
from forms import *
import xmltodict
from django.template import RequestContext, loader
from coldants.settings import LOGIN_ID, TRANSACTION_KEY, ACH_TARGET_URL
import urllib2
from django.contrib.auth.decorators import login_required
from models import CustomerProfile, AccountToken
from datetime import datetime, timedelta
from django.contrib import messages

@login_required
def fa_actions(request):
	t = loader.get_template('django_firstach/fa_actions.html')
	c = RequestContext(request)
	return HttpResponse(t.render(c))

@login_required
def customer_home(request):
	try:
		profile = CustomerProfile.objects.get(customer=request.user)
	except:
		messages.warning(request, 'You must create a profile before you can view it.')
		return redirect('createCustomerProfile')
	t = loader.get_template('django_firstach/profile_home.html')
	c = RequestContext(request, {"profile":profile})
	return HttpResponse(t.render(c))
	
@login_required
def createCustomerProfile(request):
	if CustomerProfile.objects.filter(customer=request.user).count() > 0:
		return redirect('showCustomerProfile')
	errors = None
	if request.method == 'POST':
		f = CustomerProfileForm(data=request.POST)
		if f.is_valid(): 
			t = loader.get_template('django_firstach/createCustomerProfile.xml')
			d = f.cleaned_data
			d['loginID'] = LOGIN_ID
			d['transactionKey'] = TRANSACTION_KEY
			c = RequestContext(request, {'data': d})
			req = urllib2.Request(url=ACH_TARGET_URL, data=t.render(c))
			f = urllib2.urlopen(req)
			#try:
			resp = xmltodict.parse(f.read())
			responseMessages = resp.items()[0][1]['responseMessages']
			#except:
				#unable to parse xml
			#	errors = {'message': 'Unable to parse response from FirstACH'}
			#	return render(request, 'createCustomerProfile.html', {'f': f, 'errors': errors})
			#	pass
				
			#t = loader.get_template('createCustomerResponse.html')
			#c = RequestContext(request, {'d':f.read()})
			#return HttpResponse(t.render(c))
			#responseMessages = dict(resp['createCustomerResponse']['responseMessages'])
			if responseMessages['responseCode'] == '0000':
				customer = resp.items()[0][1]['customer']
				c = CustomerProfile()
				c.customer = request.user
				c.customerToken = customer['customerToken']
				c.save()
				return redirect('showCustomerProfile')
			else:
				# TODO: log this. Heck, log everything
				errors = {'message': responseMessages['message']}
	else:
		f = CustomerProfileForm()
	return render(request, 'django_firstach/createCustomerProfile.html', {'f': f, 'errors': errors})	

@login_required
def createCustomerAccount(request, errors = None):
	try:
		profile = CustomerProfile.objects.get(customer=request.user)
	except:
		messages.warning(request, 'You must create a profile before you can create an account.')
		return redirect('createCustomerProfile')
	errors = None
	if request.method == 'POST':
		f = CustomerAccountForm(data=request.POST)
		if f.is_valid(): 
			t = loader.get_template('django_firstach/createCustomerAccount.xml')
			d = f.cleaned_data
			d['loginID'] = LOGIN_ID
			d['transactionKey'] = TRANSACTION_KEY
			d['customerToken'] = profile.customerToken
			d['authDate'] = (datetime.today()-timedelta(days=1)).strftime('%Y-%m-%d')
			c = RequestContext(request, {'data': d})
			req = urllib2.Request(url=ACH_TARGET_URL, data=t.render(c))
			
			f = urllib2.urlopen(req)
			#return HttpResponse(f)
			#try:
			resp = xmltodict.parse(f.read())
			responseMessages = resp.items()[0][1]['responseMessages']
			if responseMessages['responseCode'] == '0000':
				account = resp.items()[0][1]['account']
				if account['customerToken'] == profile.customerToken:
					token = {"last_four":d['accountNumber'][-4:], "token":account['accountToken']}
					profile.accountTokens.append(token)
					profile.save()
					return HttpResponse('ok')
				else:
					return HttpResponse('no')
			else:
				print resp
			return HttpResponse(t.render(c))
	else:
		f = CustomerAccountForm()
	return render(request, 'django_firstach/createCustomerAccount.html', {'f': f, 'errors': errors})	

@login_required
def createCustomerPayment(request):
	try:
		profile = CustomerProfile.objects.get(customer=request.user)
	except:
		return redirect('createCustomerProfile')
	errors = None
	if request.method == 'POST':
		f = CustomerPaymentForm(data=request.POST)
		if f.is_valid():
			return HttpResponse("valid bro")
	else:
		f = CustomerPaymentForm()
	return render(request, 'django_firstach/createCustomerPayment.html',  {'f': f, 'errors': errors})	
