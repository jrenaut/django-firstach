from django.conf.urls import patterns, include, url

urlpatterns = patterns('django_firstach.views',
	url(r'^$', 'fa_actions', name='actions'),
	url(r'^customer/profile/$', 'customer_home', name='showCustomerProfile'),
    url(r'^customer/profile/create/$', 'createCustomerProfile', name='createCustomerProfile'),
	url(r'^customer/account/create/$', 'createCustomerAccount', name='createCustomerAccount'),
	url(r'^customer/payment/create/$', 'createCustomerPayment', name='createCustomerPayment'),
    )
