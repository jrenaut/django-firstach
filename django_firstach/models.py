from django.db import models
from djangotoolbox.fields import EmbeddedModelField
from djangotoolbox.fields import SetField, DictField, ListField
from django.contrib.auth.models import User

class AccountToken(models.Model):
	token = models.CharField(max_length=24)
	last_four = models.CharField(max_length=4)
	default = models.BooleanField()

class CustomerProfile(models.Model):
	customer = models.ForeignKey(User)
	customerToken = models.CharField(max_length=24)
	#accountTokens = SetField(EmbeddedModelField('AccountToken'))
	accountTokens = ListField(DictField)


